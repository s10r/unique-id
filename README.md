# UniqueID Number Service

A REST API Service to generate unique, random IDs with a
checksum, like EAN.

Why?

Just for me to learn all the fancy development stuff like
unit tests, composer, integration into PhpStorm.  Maybe this
project is useful for some other, maybe not.

License: BSD

## A. Roadmap

### 1. Version 0.5

Prepare the “project”

* Setup Bitbucket Repository
* Setup README.md

### 2. Version 1.0

Generate Unique ID containing 12 + 1 digits

- PHP Unittests on Program Level
- Random Generator
- REST API with persitance in MongoDB
- PHP Unittests on REST API Level
- PHP API Documentation
- Integration into PhpStorm for testing
- Generate Composer Package
- Performance Testing (testing with > 1 Mio IDs)

### 3. Version 2.0
- Generate Unique ID containing characters allowed by
              base64.
- API rate limit
- Authentication to use the service.

## B. API Description

### 1. POST /numbers/[group]/[length]/[prefix]

Create a new Unique ID

### 2. GET /numbers/[group]

Get a list of all generated codes assigned to the
given group (mandatory parameter).  The list is
sorted by issue date, descending.

### 3. DELETE

Delete is not allowed, because it would break the
constraint that the generated Code is unique.

## C. Datamodel

One single Collection containing:

- unique_id (String) - This is the generated Unique ID
- created (Date) - This will reflect the date when the code was issued and is automatically set on creation.

## D. Technologies
1. PHP - Current Version 5.4.31
2. Slim - The Slim PHP Framework
3. Use Composer for 3rd party packages
4. Mongodb - Use current Version (2.6) for persistence.

## E. Development Environment

- Mac
- Bitnami MAMP Stack
- PhpStorm
- Debian 7.3 Linux VM for testing

